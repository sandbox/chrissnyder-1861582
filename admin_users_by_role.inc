<?php

/**
 * Gets the roles defined in system,  except anonymous and authenticated user.
 * @return array
 *  Array of roles
 */
function admin_users_by_role_get_roles(){
  $roles = user_roles(TRUE);
  unset($roles[DRUPAL_AUTHENTICATED_RID]);
  // Do not allow to delegate users to administator role. Let's keep this
  // privilege to users with 'Administer permissions' permission only.
  unset($roles[variable_get('user_admin_role', 0)]);
  return $roles;
}

/**
* Check to see if a user has been assigned a certain role.
*
* @param $role
*   The name of the role you're trying to find.
* @param $user
*   The user object for the user you're checking; defaults to the current user.
* @return
*   TRUE if the user object has the role, FALSE if it does not.
*/
function user_has_role($role, $user = NULL) {
  if ($user == NULL) {
    global $user;
  }

  if (is_array($user->roles) && in_array($role, array_values($user->roles))) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Helper function to create the role options.
 */
function admin_users_by_role_add_roles_to_form(&$form, $account) {
  $roles_current = $account->roles;

  $roles_delegate = array();
  $roles = admin_users_by_role_get_roles();
  foreach ($roles as $rid => $role) {
    if (user_access('super administer users') || user_access("assign users $role role")) {
      $roles_delegate[$rid] = isset($form['account']['roles']['#options'][$rid]) ? $form['account']['roles']['#options'][$rid] : $role;
    }
  }

  if (empty($roles_delegate)) {
    // No role can be assigned.
    return;
  }

  if (!isset($form['account'])) {
    $form['account'] = array(
      '#type' => 'value',
      '#value' => $account,
    );
  }

  // Generate the form items.
  $form['account']['roles_change'] = array(
    '#type' => 'checkboxes',
    '#title' => isset($form['account']['roles']['#title']) ? $form['account']['roles']['#title'] : t('Roles'),
    '#options' => $roles_delegate,
    '#default_value' => array_keys(array_intersect_key($roles_current, $roles_delegate)),
    '#description' => isset($form['account']['roles']['#description']) ? $form['account']['roles']['#description'] : t('Change roles assigned to user.'),
  );
}
